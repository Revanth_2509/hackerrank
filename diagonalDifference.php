<?php

    //TODO ชื่อไม่ส่ื่อความหมาย
    function diagonalDifference($arr, $n)
    {
        $primaryDiagonal = 0;
        $secondaryDiagonal = 0;

        for ($row = 0; $row < $n; $row++) {
            $secDiagonalCol = $n - $row - 1;
            $primaryDiagonal += $arr[$row][$row];
            $secondaryDiagonal += $arr[$row][$secDiagonalCol];
        }

        return abs($primaryDiagonal - $secondaryDiagonal);
    }

    $fptr = fopen(getenv("OUTPUT_PATH"), "w");

    $n = intval(trim(fgets(STDIN)));

    $arr = array();

    for ($i = 0; $i < $n; $i++) {
        $arr_temp = rtrim(fgets(STDIN));

        $arr[] = array_map('intval', preg_split('/ /', $arr_temp, -1, PREG_SPLIT_NO_EMPTY));
    }

    $result = diagonalDifference($arr, $n);

    fwrite($fptr, $result . "\n");

    fclose($fptr);
