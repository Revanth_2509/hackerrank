<?php

//TODO ชื่อไม่ส่ื่อความหมาย
//TODO . เคาะหน้าหลัง, . มีผลเรื่อง performance ของ PHP


//TODO " " to ' ' มีผลเรื่อง performance ของ PHP
function miniMaxSum($arr)
{
    // Write your code here
    $allSum = [];

    for ($i = 0; $i < count($arr); $i++) {
        $allSum[] = array_sum($arr) - $arr[$i];
    }

    $minSum = min($allSum);
    $maxSum = max($allSum);

    print($minSum . ' ' . $maxSum);
}

$arr_temp = rtrim(fgets(STDIN));

$arr = array_map('intval', preg_split('/ /', $arr_temp, -1, PREG_SPLIT_NO_EMPTY));

miniMaxSum($arr);
