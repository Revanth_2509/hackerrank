<?php

    //ชื่อไม่ส่ื่อความหมาย
    //ประกาศตัวแปลที่ใช้ซ้ำ
    function compareTriplets($a, $b)
    {
        $aliceScore = 0;
        $bobScore = 0;

        for ($index = 0; $index < 3; $index++) {
            $firstArr = $a[$index];
            $secondArr = $b[$index];
            if ($firstArr > $secondArr) {
                $aliceScore++;
            } else if ($firstArr < $secondArr) {
                $bobScore++;
            }
        }

        return [$aliceScore, $bobScore];
    }

    $fptr = fopen(getenv("OUTPUT_PATH"), "w");

    $a_temp = rtrim(fgets(STDIN));

    $a = array_map('intval', preg_split('/ /', $a_temp, -1, PREG_SPLIT_NO_EMPTY));

    $b_temp = rtrim(fgets(STDIN));

    $b = array_map('intval', preg_split('/ /', $b_temp, -1, PREG_SPLIT_NO_EMPTY));

    $result = compareTriplets($a, $b);

    fwrite($fptr, implode(" ", $result) . "\n");

    fclose($fptr);
